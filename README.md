# OpenCV Python
This is my submission for the opencv task

## Prerequisites
This script requires `opencv-python` and `numpy` to run.  
If you don't already have them, you can download them by running  
`pip install numpy`   
`pip install opencv-python`

## Running  
To run, from the root of the folder do  
`python main.py`