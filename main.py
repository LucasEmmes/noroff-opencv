import numpy as np
import cv2 as cv


def make_grayscale(frame):
    framecopy =  cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    return cv.cvtColor(framecopy, cv.COLOR_GRAY2BGR)

def draw_text(frame):
    height, width, _ = frame.shape
    framecopy = frame
    framecopy = cv.circle(framecopy, (width//2, height//2), height//4, (0, 255, 255), -1)
    framecopy = cv.circle(framecopy, (width//2-width//16, height//2-height//16), height//16, (0, 0, 0), -1)
    framecopy = cv.circle(framecopy, (width//2+width//16, height//2-height//16), height//16, (0, 0, 0), -1)
    framecopy = cv.ellipse(framecopy, (width//2, height//2), (height//6, height//6), 0, 0, 180, (0,0,0), 5)
    framecopy = cv.putText(framecopy, 'c bus bus', (int(width*0.1), int(height*0.9)), cv.FONT_HERSHEY_SIMPLEX, 1, (0, 100, 255), 2, cv.LINE_4)
    return framecopy


def superimpose(frame):
    height, width, _ = frame.shape
    linus = cv.imread("wide_linus.jpg")
    linus = cv.resize(linus, (width, height))
    return cv.addWeighted(frame, 0.7, linus, 0.3, 0)


def transpose(frame):
    height, width, _ = frame.shape
    framecopy = frame
    framecopy[int(height*0.25):int(height*0.75)][int(width*0.25):int(width*0.75)] = cv.flip(framecopy[int(height*0.25):int(height*0.75)][int(width*0.25):int(width*0.75)], 1)
    return framecopy


def mask_color(frame):
    framecopy = frame
    hsv = cv.cvtColor(framecopy, cv.COLOR_BGR2HSV)
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])
    mask = cv.inRange(hsv, lower_blue, upper_blue)
    framecopy[mask>0] = (0, 0, 255)
    return framecopy


def main():

    cap = cv.VideoCapture(0)

    if not cap.isOpened():
        print("Cannt open camera")
        exit()

    out = cv.VideoWriter("output.avi", cv.VideoWriter_fourcc(*'XVID'), 20.0, (640, 480))
    framecounter = 0
    while True:
        framecounter += 1
        ret, frame = cap.read()

        if not ret:
            print("Cannot receive frame")
            break

        modifiedframe = frame
        if framecounter < 300: modifiedframe = make_grayscale(frame)
        elif framecounter < 600: modifiedframe = draw_text(frame)
        elif framecounter < 900: modifiedframe = superimpose(frame)
        elif framecounter < 1200: modifiedframe = transpose(frame)
        elif framecounter < 1500: modifiedframe = mask_color(frame)
        else: break

        out.write(modifiedframe)
        cv.imshow('frame', modifiedframe)
        if cv.waitKey(1) == ord("q"):
            break

    cap.release()
    out.release()
    cv.destroyAllWindows()

if __name__=="__main__":main()